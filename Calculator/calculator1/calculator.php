<?php

class calculator {
    private $num1="";
    private $num2="";
    private $operator="";
    public $result="";
    
    public function setData($num1,$num2,$operator) {
        $this->num1 = $num1;
        $this->num2 = $num2;
        $this->operator = $operator;
    }
    public function getData() {
        if($this->operator == "add"){
            $this->add();   
        }
        if ($this->operator == "sub"){
            $this->sub();
        }
        if ($this->operator == "mul"){
            $this->mul(); 
        }
        if($this->operator == "div"){
            $this->div();
        }
    }
    public function add() {
        echo $this->result = $this->num1 + $this->num2;
        
    }
    public function sub() {
        echo $this->result = $this->num1 - $this->num2;
        
    }
    public function mul() {
        echo $this->result = $this->num1 * $this->num2;
        
    }
    public function div() {
        echo $this->result = $this->num1 / $this->num2;
    }
}
